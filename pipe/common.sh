#!/bin/bash

# Begin Standard 'imports'
set -e
set -o pipefail

gray="\\e[94m"
blue="\\e[36m"
red="\\e[31m"
green="\\e[32m"
reset="\\e[0m"

info() { echo -e "${blue}INFO: $*${reset}"; }
error() { echo -e "${red}ERROR: $*${reset}"; }
debug() {
    if [[ "${DEBUG}" == "true" ]]; then
        echo -e "${gray}DEBUG: $*${reset}";
    fi
}

success() { echo -e "${green}✔ $*${reset}"; }
fail() { echo -e "${red}✖ $*${reset}"; exit 1; }

## Enable debug mode.
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}

# Execute a command, saving its output and exit status code, and echoing its output upon completion.
# Globals set:
#   status: Exit status of the command that was executed.
#   output: Output generated from the command.
#
run() {
  echo "$@"
  set +e
  output=$("$@" 2>&1)
  status=$?
  set -e
  echo "${output}"
}

# https://support.atlassian.com/bitbucket-cloud/docs/advanced-techniques-for-writing-pipes/
init_array_var() {
  local array_var=${1}
  local count_var=${array_var}_COUNT
  for (( i = 0; i < ${!count_var:=0}; i++ ))
  do
    eval ${array_var}[$i]='$'${array_var}_${i}
  done
}

# End standard 'imports'
