#!/bin/bash

set -eo pipefail

source "$(dirname "$0")/common.sh"

SCANNER_LOGS="${BITBUCKET_PIPE_STORAGE_DIR}/sonarcloud-scan.log"

parse_environment_variables() {
  EXTRA_ARGS=${EXTRA_ARGS:=""}
  SONAR_TOKEN=${SONAR_TOKEN:?'SONAR_TOKEN variable missing.'}
  BITBUCKET_CLONE_DIR=${BITBUCKET_CLONE_DIR:?'BITBUCKET_CLONE_DIR variable missing.'}
}

parse_environment_variables

if [ ! -z ${EXTRA_ARGS_COUNT} ]; then
  init_array_var 'EXTRA_ARGS';
else
  IFS=' ' read -r -a EXTRA_ARGS <<< "${EXTRA_ARGS}"
fi

ALL_ARGS=("${EXTRA_ARGS[@]}")

if [[ "${DEBUG}" == "true" ]]; then
  ALL_ARGS+=("-X")
  debug "EXTRA_ARGS: ${EXTRA_ARGS}"
  debug "ALL_ARGS:${ALL_ARGS}"
  debug "SONAR_SCANNER_OPTS: ${SONAR_SCANNER_OPTS}"
fi

sonar-scanner "${ALL_ARGS[@]}" 2>&1 | tee "${SCANNER_LOGS}" || {
  fail "SonarQube Cloud analysis failed. (exit code = $?)"
}

if grep -q "EXECUTION SUCCESS" "${SCANNER_LOGS}"
then
  success "SonarQube Cloud analysis was successful."
else
  fail "SonarQube Cloud analysis failed."
fi
