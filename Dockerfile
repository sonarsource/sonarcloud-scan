FROM sonarsource/sonar-scanner-cli:11.2

# Run the pipe as ROOT user, to avoid permission issues on $BITBUCKET_PIPE_STORAGE_DIR
# https://community.atlassian.com/t5/Bitbucket-questions/quot-Permission-denied-quot-when-writing-to-BITBUCKET-PIPE/qaq-p/2452665#U2829731
USER 0

COPY --chown=scanner-cli:scanner-cli pipe /
RUN chmod a+x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
